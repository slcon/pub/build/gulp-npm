<!--****************************************************************************
* $Id: README.md 72ef34e 2022-04-09 11:40:46 -0700 sthames42 $
*****************************************************************************-->
# gulp-npm

Provides a base framework of common functions and predefined tasks for use in 
[`gulpfile.js`][Gulp] files that build [npm] packages and packages that may be 
installed using either npm or [bower](https://bower.io/).

<!-- toc -->

- [Installation](#installation)
- [Usage](#usage)
  * [Task Usage Help](#task-usage-help)
  * [Available Tasks](#available-tasks)
    + [build-package.json](#build-packagejson)
    + [publish](#publish)
    + [publish-bower](#publish-bower)
    + [set-bower-token](#set-bower-token)
    + [set-registry](#set-registry)
    + [update-bower.json](#update-bowerjson)
- [API](#api)
  * [Functions](#functions)
    + [addTasks](#addtasks)
    + [getBowerInfo](#getbowerinfo)
    + [makeTokenKey](#maketokenkey)
    + [run](#run)
    + [useGulp](#usegulp)
- [Building This Package](#building-this-package)
  * [Build Commands](#build-commands)
  * [TL;DR](#tldr)
  * [`gulp-common` Dependency](#gulp-common-dependency)
- [See Also](#see-also)

<!-- tocstop -->

## Installation

  1) Set the endpoint for the [package registry] using [npm].
  1) Install this package as a development dependency:

```bash
npm install --save-dev @slcpub/gulp-npm
```
[package registry]: https://gitlab.com/slcon/pub/registry#npm

## Usage

```js
const gulp = require('gulp');
const npm  = require('@slcpub/gulp-npm').useGulp(gulp);
npm({options});
npm.addTasks([task...]);
```
  - `npm({options})` sets any desired options.
  - `npm.addTasks(task...)` adds some or all of the available tasks to 
    `gulp` using `gulp.task()`.

### Task Usage Help

See [Task Usage Help in `gulp-common`][usage-help] for information on creating and displaying
usage help content for tasks.

[usage-help]: https://gitlab.com/slcon/pub/build/gulp-common#task-usage-help

### Available Tasks

These tasks may be added to the Gulp instance by calling [addTasks].

#### build-package.json

Copies `package.json` to the `dist` folder for publishing. Only properties required
for use of the package are included. All other properties are stripped.

#### publish

Publishes to the package registry. Must be run after `build` and publishes only the 
`dist` folder. Runs [build-package.json](#build-package-json) first.

#### publish-bower

Publishes the package to a private bower registry created with the npm [private-bower]
package. This must be run after the package has been built and the local repo has been
pushed to the remote. Reads the server URL from `.bowerrc#registry` and the package
repo URL from `package.json#repository#url`. For a private repo, `gulp set-bower-token`
must be run first to set the required read access token string in the repo URL.

[private-bower]: https://www.npmjs.com/package/private-bower

#### set-bower-token

Takes input of an access token that will allow cloning of the package repo. The token 
is added to `npm config` as a token key with `_readToken` as the token property 
name for use in `gulp publish-bower`.

If the user cancels (Ctrl-C), no action is taken. If an empty token is entered, npm 
config value is removed. 

#### set-registry

Updates npm configuration for publishing to the package registry. Reads `package.json` 
for `name` to retrieve the package `@scope`, `publishConfig` to get the Project-Level
Endpoint URL of the package registry, and sets npm config values to allow publishing of
the package to npm with `gulp publish`.

Takes input of the Registry Authentication Token from the user. If the user cancels
(Ctrl-C), no action is taken. If an empty token is entered, npm config values are removed.

#### update-bower.json

Updates `bower.json` with properties from `package.json`. Must be run before commit to
get property updates, including version, from `package.json`.

## API

### Functions

#### addTasks 

Adds [available tasks](#available-tasks) to the [Gulp instance](#useGulp).

  - ***parameters***
    - `{string[]|string...}`
      Name of the task to add. All tasks are added if no names are supplied.

#### getBowerInfo

Returns `bower.json` content. Throws fatal if `name` or `version` is missing.

  - ***returns***  
    - `{object}` `bower.json` content.

#### makeTokenKey

Builds npm token key for use in setting authentication tokens in `npm config`.
Token keys are endpoint URLs without the protocol (everything after and
including the double-slash (//)) appended with ':_authToken' or some other
property.

  - ***parameters***
    - `{string}`
      Endpoint URL.
    - `{string}`
      Token property name.
  - ***returns***  
    - `{object}` 
      Authentication token key.

#### run

Runs `npm` on the command line.

  - ***parameters***
    - `{string}` `npm` command parameters.
    - `{options}`  
      - **quiet**    `{boolean}`  
        `true`=do not echo successful command output.
      - **silent**   `{boolean}`  
        `true`=do not echo command name or successful output.
      - **nonfatal** `{boolean}`  
        `true`=reject errors with error information.  
        `false`=reject errors with fatal error message.   
      - Remaining options will be passed to [`child_process.exec()`].
  - ***returns***  
    - `{promise}` Resolved with command output, rejeted with fatal error info.

#### useGulp

Assigns the Gulp instance to use when adding and running tasks.

  - ***parameters***
    - `{object}` Gulp instance.
  - ***returns***  
    - `{function}` Self-reference.
  - ***usage***
    ```js
    const gulp = require('gulp');
    const npm  = require('@slcpub/gulp-npm').useGulp(gulp);
    ```

## Building This Package

### Build Commands

**`gulp set-registry`** takes input of a [personal access token] or [deploy token] and configures npm for 
[publishing to the package registry][publishing].

[**`npm version`**][npm version] updates **`package.json#version`**, runs **`gulp build`**, commits to
the local repo, and creates a tag for the version.

**`gulp undo-version`** may be run immediately after [npm version]. It reverts the changes and restores
the repository to it's prior state.

[**`npm publish`**][npm publish] is run after **`npm version`** to publish the package to npm registry as defined in 
**`package.json#publishConfig`**.

### TL;DR

After testing is complete and the package is ready to publish:

1) Run [**`npm version`**][npm version]
   * *preversion*: Runs **`npm test`** to run the library tests,
   * Updates the version number in `package.json`,
   * Commits changes and creates a version tag.

1) If, on second thought, the package is not ready,
   * Run **`gulp undo-version`** to remove the tag and restore the repository.

1) If ready to publish,
   * Run **`gulp push`** to update the remote repo with the new version.

1) Run **`gulp set-registry`** to configure npm for publishing to the package registry. This will require
   a [personal access token] or [deploy token].
   
1) Run [**`npm publish`**][npm publish] to publish the package to npm.


### `gulp-common` Dependency

This package is inter-dependent with [`gulp-common`][gulp-common] meaning 
they each have the other as a package dependency.

When this package is modified:

  1) Commit, version, and publish `gulp-npm`.  
     **Do not push to origin**.
  1) Update `gulp-common` to use the new `gulp-npm`.
  1) Commit, version, push, and publish `gulp-common`.
  1) Run `gulp undo-version` for `gulp-npm`.
  6) Update `gulp-npm` to use the new `gulp-common`.
  7) Remove the previously published `gulp-npm`.
  1) Commit, version, push, and publish `gulp-npm`.


## See Also

  - [Gulp]
  - [npm]
  - [npm Packages in the Package Registry][npm registry]
  - [npm Package Scopes][npm scope]
  - [Personal Access Token][personal access token]
  - [Deploy Tokens][deploy token]
  - [`npm version` command][npm version]
  - [bower.json specification][bower.json]
  - [The npm `private-bower` package][private-bower].

[gulp]:                  https://gulpjs.com/docs/en/getting-started/javascript-and-gulpfiles
[gulp-common]:           https://gitlab.com/slcon/pub/build/gulp-common
[npm]:                   https://docs.npmjs.com/about-npm
[package registry]:      https://docs.gitlab.com/ee/user/packages/package_registry/
[publishing a package]:  https://docs.gitlab.com/ee/user/packages/npm_registry/#publish-an-npm-package
[npm registry]:          https://docs.gitlab.com/ee/user/packages/npm_registry/    
[npm scope]:             https://docs.npmjs.com/cli/v6/using-npm/scope
[personal access token]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens
[deploy token]:          https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html
[npm config]:            https://docs.npmjs.com/cli/v8/commands/npm-config
[npm publish]:           https://docs.npmjs.com/cli/v8/commands/npm-publish
[npm version]:           https://docs.npmjs.com/cli/v6/commands/npm-version
[bower.json]:            https://github.com/bower/spec/blob/master/json.md
[semver versioning]:     https://semver.org/
[private-bower]:         https://www.npmjs.com/package/private-bower
