/*******************************************************************************
* $Id$
*******************************************************************************/
(function() { 'use strict';

const fs     = require('fs');
const common = require('@slcpub/gulp-common');
const $self  = module.exports = common.makeModule(__dirname); 

/*-----------*/
/* Functions */
/*-----------*/
Object.assign($self,
  {
  /*****************************************************************************************
  * getBowerInfo */
  /**
  * Returns `bower.json` content. Throws fatal if `name` or `version` is missing.
  *
  * @return {object}  Package bower information.
  *****************************************************************************************/
  getBowerInfo: () =>
    {
    const info = JSON.parse(fs.readFileSync("./bower.json").toString('utf-8'));
    if (!!info.name && !!info.version)
      return info;
    throw common.fatal("Unable to parse 'bower.json'");
    },

  /*****************************************************************************
  * makeTokenKey */
  /**
  * Builds npm token key for use in setting authentication tokens in `npm config`.
  * Token keys are endpoint URLs without the protocol (everything after and
  * including the double-slash (//) appended with ':_authToken' or some other
  * property.
  *
  * @param   {string}  url   Endpoint URL.
  * @param   {string}  prop  Name to add to end of `url`.
  * @returns {strint}        Token key. 
  ******************************************************************************/
  makeTokenKey: (url, prop) =>
    {
    /*------------------------------------*/
    /* Token key is endpoint URL from     */
    /* double-slash (//) + ":" + `prop`.
    /*------------------------------------*/
    var m = (url||'').match(/\/\/.+$/);
  
    if (!m)
      throw `'${url}' is not a valid endpoint URL'.`;
  
    return m.pop()+`:${prop}`;
    },

  /*****************************************************************************
  * run */
  /**
  * Runs `npm` on the command line.
  * 
  * In version 6.x (legacy), npm logs to stderr regardless of the message level. 
  * `notice` logs to stderr just as `error` does. This may be changed in the 
  * latest version of npm.
  * https://github.com/npm/npm/issues/118#issuecomment-325440
  * 
  * Gulp checks for the presence of a `showStack` property on `Error` objects 
  * passed to a task callback or in the payload of a rejected promise. If this 
  * value is not an `Error` object, Gulp throws a format error. This may be 
  * changed in the latest version of Gulp.
  * https://stackoverflow.com/a/39093327/2245849
  *
  * `options` will be passed to `child_process.exec()` but also may include:
  *   - {boolean} quiet    `true`=do not echo successful command output.
  *   - {boolean} silent   `true`=do not echo command name or successful output.
  *   - {boolean} nonfatal `true`=reject errors with error information.
  *                        `false`=reject errors with fatal error message.
  *
  * @param   {string}  parms  `npm` command parameters.
  * @param   {string}  cmd    Command to execute.
  * @param   {object}  opts   `child_process` + additional options (optional).
  * @returns {Promise} Resolved with command output, rejected with fatal error info. 
  ******************************************************************************/
  run: (parms, opts) => common.run(`npm ${parms}`, opts)
  });
})();
