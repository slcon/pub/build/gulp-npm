/*****************************************************************************************
* $Id$
/**
* Updates npm configuration for publishing to the scoped registry. Reads `package.json` 
* for `name` to retrieve the package @scope and `publishConfig` to get the Project-Level
* Endpoint URL of the package registry and sets npm config values to allow publishing of
* the package to npm with `gulp publish`.
*
* Takes input of the Registry Authentication Token from the user. If the user cancels
* (Ctrl-C), no action is taken. If an empty token is entered, npm config values are removed.
*
* @task  {set-registry}
* @group {Supporting Tasks}
* @order {200}
* 
* npm Config Values
*   @scope:registry              - Project-Level Endpoint URL 
*   @scope:<EndPoint>:_authToken - Registry Authentication Token
* where:
*   EndPoint = Project-Level Endpoint URL without protocol (https:).
*
* These values may also be manually maintained in `.npmrc`.
*   
* see: https://docs.gitlab.com/ee/user/packages/npm_registry/#use-the-gitlab-endpoint-for-npm-packages
*****************************************************************************************/
(function() { 'use strict';

const colors   = require('ansi-colors');
const os       = require('os');
const path     = require('path');
const process  = require('process');
const Q        = require('q');
const readline = require('readline');
const common   = require('@slcpub/gulp-common');
const npm      = require('../index');

module.exports = (() =>
  {
  /*-----------------------------------------------------------*/
  /* Load `package.json` and get @scope from the package name. */
  /*-----------------------------------------------------------*/
  var pkgInfo = common.getPackageInfo();
  var scope   = (pkgInfo.name.match(/^@[^\/]+(?=\/)/)||[]).pop();

  if (!scope)
    throw `'${pkgInfo.name}' has no scope to register.`;

  /*----------------------------------------------*/
  /* Build the registry key (@scope:registry) and */
  /* get the endpoint URL from `publishConfig`.   */
  /*----------------------------------------------*/
  if (!pkgInfo.publishConfig)
    throw "'publishConfig' not found in 'package.json'.";

  var registryKey = scope+':registry';
  var endpointURL = pkgInfo.publishConfig[registryKey];

  if (!endpointURL)
    throw `Registry key '${registryKey}' not found in 'package.json[publishConfig]'.`;

  /*------------------------------------*/
  /* Token key is endpoint URL from     */
  /* double-slash (//) + ":_authToken". */
  /*------------------------------------*/
  var tokenKey = npm.makeTokenKey(endpointURL, '_authToken');

  /*----------------------------------------------------------------*/
  /* Get the access token from the user. Cancel on Ctrl-C.          */
  /*                                                                */
  /* Delete `~/.config/configstore` if setting the token fails.     */
  /* https://github.com/npm/npm/issues/17946#issuecomment-352283154 */
  /*----------------------------------------------------------------*/
  console.log(colors.yellow('\nThis sometimes fails with `npm update check failed` message. Try it a couple of times.'));
  console.log(colors.yellow(`If the error persists, try deleting '${path.join(os.homedir(), '.config/configstore')}'.\n`));

  var rl = readline.createInterface({input:process.stdin, output:process.stdout});
  
  var r = Q.promise((resolve, reject) =>
    {
    rl.on      ('SIGINT', reject);
    rl.question("Enter Authentication Token (Press Enter to delete, Ctrl-C to cancel): ", resolve)
    })
  .finally(() => rl.close());

  /*--------------------------------------*/
  /* Set config values if token provided. */
  /* Remove values if token is empty.     */
  /*--------------------------------------*/
  return r.then((answer) => 
    Q.all(answer ? [ npm.run(`config set    ${registryKey}=${endpointURL}`), npm.run(`config set    ${tokenKey}=${answer}`) ]
                 : [ npm.run(`config delete ${registryKey}`),                npm.run(`config delete ${tokenKey}`) ]));
  });
})();

