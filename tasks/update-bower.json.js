/*****************************************************************************************
* $Id$
/**
* Updates `bower.json` with properties from `package.json`. Must be run before commit to
* get property updates, including version, from `package.json`.
*
* @task  {update-bower.json}
* @group {Supporting Tasks}
* @order {200}
*****************************************************************************************/
(function() { 'use strict';

var fs     = require('fs');
var Q      = require('q');
var common = require('@slcpub/gulp-common');

module.exports = () =>
  {
  const pkg   = JSON.parse(fs.readFileSync("./package.json").toString('utf-8'));
  const bower = JSON.parse(fs.readFileSync("./bower.json")  .toString('utf-8'));

  /*---------------------------------*/
  /* Remove scope from package name. */
  /*---------------------------------*/
  const name = pkg.name.match(/^(?:@[^\/]+\/)?(.+)$/).pop();

  if (bower.name !== name || bower.description !== pkg.description || bower.version !== pkg.version)
    {
    Object.assign(bower, 
      {
      name:        name,
      description: pkg.description,
      version:     pkg.version
      });
  
    fs.writeFileSync("./bower.json", Buffer.from(common.normalizeEOL(JSON.stringify(bower, null, 2)), "utf-8"));
    }

  return Q.resolve();
  };
})();

