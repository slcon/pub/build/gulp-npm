/*****************************************************************************************
* $Id$ */
/**
* Publishes the package to a private bower registry created with the npm `private-bower`
* package. This must be run after the package has been built and the local repo has been
* pushed to the remote. Reads the server URL from `.bowerrc#registry` and the package
* repo URL from `package.json#repository#url`. For a private repo, `gulp set-bower-token` 
* must be run first to set the required read access token string in the repo URL.
*
* @task  {publish-bower}
* @group {Build Tasks}
* @order {100}
* 
* A post request is made to the `private-bower` repo to provide the package name and repo
* URL and then a `details` request is made to verify the URL accuracy.
* https://www.npmjs.com/package/private-bower
*****************************************************************************************/
(function() { 'use strict';

var fs     = require('fs');
var http   = require('http');
var path   = require('path');
var Q      = require('q');
var common = require('@slcpub/gulp-common');
var npm    = require('../index');

module.exports = () => Q.Promise((resolve, reject) =>
  {
  /*-----------------------------------------------------*/
  /* Get the bower server host from `.bowerrc#registry`. */
  /*-----------------------------------------------------*/
  if (!fs.existsSync('./.bowerrc'))
    reject(common.fatal('No `.bowerrc` file found.'));

  const bowerrc = JSON.parse(fs.readFileSync("./.bowerrc").toString('utf-8'));
  const server  = bowerrc.registry;

  if (!server)
    reject(common.fatal('No `registry` value found in `.bowerrc` file.'));

  /*-----------------------------------------------*/
  /* Read out the package info and check for an    */
  /* access token string set by `set-bower-token`. */
  /*-----------------------------------------------*/
  const pkgInfo   = common.getPackageInfo();
  const bowerInfo = npm.getBowerInfo();
  const pkgName   = bowerInfo.name;
  const repoURL   = (pkgInfo.repository||{}).url;
  const tokenKey  = npm.makeTokenKey(repoURL, 'bowerToken');

  npm.run(`config get ${tokenKey}`, {silent:true}).then(token =>
    {
    const gitURL = !!token && token !== 'undefined' 
                 ? repoURL.replace('://', `://${token}@`) 
                 : repoURL;

    /*---------------------------------------------------*/
    /* Send POST request to `<server>/packages` with     */
    /* package name and git URL to register the package. */
    /* `private-bower` returns 201 if successful.        */
    /*---------------------------------------------------*/
    const json     = JSON.stringify({ name:pkgName, url:gitURL });
    const bowerURL = new URL(path.posix.join(server, 'packages'));
    const opts     =
      {
      method:  'POST',
      headers: 
        {
        "Content-type": "application/json",
        "Content-length": Buffer.byteLength(json)
        }
      };
  
    const req = http.request(bowerURL, opts, (res) => 
      {
      res.resume();

      if (res.statusCode !== 201)
        reject(common.fatal(`Publish failed, status code = ${res.statusCode}.`));

      /*---------------------------------------------------------------*/
      /* Send GET request to `<server>/packages/<packageName>/details` */
      /* to verify the package URL is correct. `private-bower` returns */
      /* 200 for success and 404 if the URL is bad.                    */
      /*---------------------------------------------------------------*/
      http.get(new URL(path.posix.join(bowerURL.toString(), pkgName, 'details')), (res) => 
        {
        res.resume();
        if (res.statusCode != 200)
          reject(common.fatal(`Publish failed, status code = ${res.statusCode}.\nPackage could not be retrieved with the URL provided.`));
        resolve();
        });
      });
    req.write(json);
    req.end();
    })
  .catch(reject);
  });
})();
