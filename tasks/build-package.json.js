/*****************************************************************************************
* $Id$
/**
* Copies `package.json` to the `dist` folder for publishing. Only properties required
* for use of the package are included. All other properties are stripped.
*
* @task  {build-package.json}
* @group {Supporting Tasks}
* @order {200}
*****************************************************************************************/
(function() { 'use strict';

var fs     = require('fs');
var glob   = require('glob');
var path   = require('path');
var Q      = require('q');
var common = require('@slcpub/gulp-common');

module.exports = (() =>
  {
  /*-------------------------------------------------------------*/
  /* Read out `package.json` and filter out unwanted properties. */
  /*-------------------------------------------------------------*/
  const props  = ['author', 'bugs', 'dependencies', 'description', 'homepage', 'license', 'name', 'repository', 'version'];
  const source = JSON.parse(fs.readFileSync("./package.json").toString('utf-8'));
  const pkg    = Object.keys(source).filter(key => props.includes(key)).reduce((obj, key) => { obj[key] = source[key]; return obj; }, {});

  /*---------------------------------------------------------*/
  /* Remove folder names from `main` and `files` properties. */
  /* These files will be in the published folder.            */
  /*---------------------------------------------------------*/
  if (source.main)
    pkg.main = path.win32.basename(source.main);
  
  if (source.files)
    pkg.files = source.files.reduce((a, g) => a.concat(glob.sync(g).map((fn) => path.win32.basename(fn))), []);

  /*------------------------------------*/
  /* Write `package.json` to `dist`.    */
  /* `dist` folder is ready to publish. */
  /*------------------------------------*/
  fs.writeFileSync("./dist/package.json", Buffer.from(common.normalizeEOL(JSON.stringify(pkg, null, 2)), "utf-8") );

  return Q.resolve();
  });
})();

