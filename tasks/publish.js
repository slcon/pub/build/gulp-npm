/*****************************************************************************************
* $Id$ */
/**
* Publishes to npm. Must be run after `build` and publishes only the `dist` folder.
*
* @task  {publish}
* @group {Build Tasks}
* @order {100}
*****************************************************************************************/
(function() { 'use strict';

var fs  = require('fs');
var Q   = require('q');
var npm = require('../index');

module.exports = () => npm.run('publish dist')
           .then(() => Q.resolve(fs.unlinkSync("./dist/package.json")));

module.exports.dependencies = ['build-package.json'];
})();

