/*****************************************************************************************
* $Id$
/**
* Takes input of an access token string that, when embedded in the repository URL, will 
* allow cloning of the package repo. The token string is added to `npm config` as a token 
* key with `bowerToken` as the token property name for use in `gulp publish-bower`.
* 
* The token string is embedded in the repo URL as `https://<tokenString>@host.com/...`
* and must be formatted according the requirements of the remote git server. For example, 
* GitHub requires tokens to be formatted as `oauth2:<token>` while GitHub only requires 
* the actual token.
* 
* If the user cancels (Ctrl-C), no action is taken. If an empty token is entered, npm 
* config value is removed. 
*
* @task  {set-bower-token}
* @group {Supporting Tasks}
* @order {200}
*****************************************************************************************/
(function() { 'use strict';

var colors   = require('ansi-colors');
var process  = require('process');
var Q        = require('q');
var readline = require('readline');
var common   = require('@slcpub/gulp-common');
var npm      = require('../index');

module.exports = (() =>
  {
  /*---------------------------------------------------------*/
  /* Load `package.json`, get repo URL, and build token key. */
  /*---------------------------------------------------------*/
  var pkgInfo = common.getPackageInfo();
  var repoURL = (pkgInfo.repository||{}).url;

  if (!repoURL)
    throw `'${pkgInfo.name}' has no repository URL.`;

  var tokenKey = npm.makeTokenKey(repoURL, 'bowerToken');

  /*-------------------------------------------------------*/
  /* Get the access token from the user. Cancel on Ctrl-C. */
  /*-------------------------------------------------------*/
  console.log(colors.yellow('\nThis often fails the first time because `npm` is stupid can\'t seem to figure out if it has access.'));
  console.log(colors.yellow('Try it a couple of times, if necessary.\n'));

  var rl = readline.createInterface({input:process.stdin, output:process.stdout});
  
  var r = Q.promise((resolve, reject) =>
    {
    rl.on      ('SIGINT', reject);
    rl.question("Enter access token string (Press Enter to delete, Ctrl-C to cancel): ", resolve)
    })
  .finally(() => rl.close());

  /*--------------------------------------*/
  /* Set config values if token provided. */
  /* Remove value if token is empty.      */
  /*--------------------------------------*/
  return r.then((answer) => (answer ? npm.run(`config set ${tokenKey}=${answer}`)
                                    : npm.run(`config delete ${tokenKey}`)));
  });
})();

